using System.Collections.Generic;
using System.Linq;

namespace MiniGamesBot.Extensions
{
    public static class DictionaryExtensions
    {
        public static Dictionary<TKey, TValue> ShallowClone<TKey, TValue>(this IDictionary<TKey, TValue> dictionary)
            where TKey : notnull
        {
            return dictionary.ToDictionary(entry => entry.Key, entry => entry.Value);
        }
    }
}