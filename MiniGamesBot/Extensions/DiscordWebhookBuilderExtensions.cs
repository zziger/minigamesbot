using System.Collections.Generic;
using System.Linq;
using DSharpPlus.Entities;

namespace MiniGamesBot.Extensions
{
    public static class DiscordWebhookBuilderExtensions
    {
        public static DiscordMessageBuilder ToMessageBuilder(this DiscordWebhookBuilder webhookBuilder)
        {
            var builder = new DiscordMessageBuilder();
            
            if (webhookBuilder.Content is not null)
                builder.WithContent(webhookBuilder.Content);
            
            foreach (var component in webhookBuilder.Components)
                builder.AddComponents(component.Components);
            
            builder.AddEmbeds(webhookBuilder.Embeds);

            return builder;
        }

        public static DiscordWebhookBuilder AddSelect(this DiscordWebhookBuilder builder, string id, string label, string noElementsLabel,
            IList<DiscordSelectComponentOption> options, bool disabled = false, int minOptions = 1, int maxOptions = 1)
        {
            if (!options.Any())
            {
                options = new List<DiscordSelectComponentOption>
                {
                    new(noElementsLabel, "-")
                };
                disabled = true;
            }

            builder.AddComponents(new DiscordSelectComponent(id, label, options, disabled, minOptions, maxOptions));

            return builder;
        }
    }
}