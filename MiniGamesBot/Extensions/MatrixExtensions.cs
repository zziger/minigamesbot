using System;
using System.Drawing;
using System.Linq;

namespace MiniGamesBot.Extensions
{
    public static class MatrixExtensions
    {
        public static T[,] RotateMatrixClockwise<T>(this T[,] oldMatrix)
        {
            var height = oldMatrix.GetLength(0);
            var width = oldMatrix.GetLength(1);
            T[,] newMatrix = new T[width, height];
            
            for (var row = 0; row < height; row++)
            {
                for (var column = 0; column < width; column++)
                {
                    newMatrix[column, height - row - 1] = oldMatrix[row, column];
                }
            }
            
            return newMatrix;
        }
        
        public static TOut[,] Apply<TIn, TOut>(this TIn[,] oldMatrix, Func<TIn, TOut> fn)
        {
            var height = oldMatrix.GetLength(0);
            var width = oldMatrix.GetLength(1);
            TOut[,] newMatrix = new TOut[height, width];
            
            for (int row = 0; row < height; row++)
            {
                for (int column = 0; column < width; column++)
                {
                    newMatrix[row, column] = fn(oldMatrix[row, column]);
                }
            }
            
            return newMatrix;
        }
        
        public static T[,] Add<T>(this T[,] destinationMatrix, T[,] sourceMatrix, Point pos, Func<Point, T, bool> fn)
        {
            var destinationWidth = destinationMatrix.GetLength(1);
            var destinationHeight = destinationMatrix.GetLength(0);
            var width = sourceMatrix.GetLength(1);
            var height = sourceMatrix.GetLength(0);
            var x = pos.X;
            var y = pos.Y;

            for (var row = 0; row < height; row++)
            {
                for (var column = 0; column < width; column++)
                {
                    if (column + x < 0 || column + x > destinationWidth - 1 || row + y < 0 || row + y > destinationHeight - 1) continue;
                    var source = sourceMatrix[row, column];
                    if (!fn(new Point(column, row), source)) continue;
                    destinationMatrix[row + y, column + x] = source;
                }
            }

            return destinationMatrix;
        }
        
    }
}