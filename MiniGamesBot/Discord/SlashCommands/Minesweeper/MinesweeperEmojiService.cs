using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;
using MiniGamesBot.Discord.Games;
using MiniGamesBot.Discord.SlashCommands.Chess;

namespace MiniGamesBot.Discord.SlashCommands.Minesweeper
{
    public interface IMinesweeperEmojiService
    {
        Task UpdateEmojis(BaseDiscordClient client);
        DiscordEmoji GetEmoji(string emoji);
    }

    public class MinesweeperEmojiService : IMinesweeperEmojiService
    {
        private Dictionary<string, DiscordEmoji> _emojis = new();
        
        private static readonly List<ulong> EmojiGuilds = new()
        {
            860420354832072714
        };
        
        public async Task UpdateEmojis(BaseDiscordClient client)
        {
            if (this._emojis.Count > 0) return;
            foreach (var discordGuild in client.Guilds.Values.Where(g => EmojiGuilds.Contains(g.Id)))
            {
                foreach (var emoji in await discordGuild.GetEmojisAsync())
                {
                    if (!emoji.Name.StartsWith("m")) continue;
                    this._emojis.Add(emoji.Name, emoji);
                }
            }
        }
        
        public DiscordEmoji GetEmoji(string emoji)
        {
            return this._emojis[emoji];
        }
    }
}