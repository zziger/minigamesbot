using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.SlashCommands;
using JetBrains.Annotations;
using MiniGamesBot.Discord.Games;
using MiniGamesBot.Discord.Games.Tetris;
using MiniGamesBot.Discord.SlashCommands.Minesweeper;
using MiniGamesBot.Extensions;
using Timer = System.Timers.Timer;

namespace MiniGamesBot.Discord.SlashCommands
{
    public class TetrisCommand : ApplicationCommandModule
    {
        private static string[] Emojis =
        {
            "▪️", "⬜", "▫️", "🟥", "🟧", "🟨", "🟩", "🟦", "🟪", "🟫"
        };

        private object _lock = new();
        private readonly DiscordClient _discordClient;
        private Tetris _game = null!;
        private DiscordUser _player = null!;
        private Point _current = new Point(0, 0);
        private ulong _interactionId = 0;
        private DiscordInteraction _interaction = null!;
        private bool _currentMode = true;
        private bool _rtlMenu = false;

        private static readonly Dictionary<ulong, DiscordUser> PlayingGuilds = new();

        private Timer _timer = new(1000)
        {
            AutoReset = false
        };

        private int _pauseLocked = 0;
        private bool _paused = false;


        public TetrisCommand(DiscordClient discordClient)
        {
            this._discordClient = discordClient;
        }

        private async Task OnInteraction(DiscordClient sender, ComponentInteractionCreateEventArgs interaction)
        {
            if (interaction.Message.Interaction.Id != this._interactionId) return;
            if (this._player.Id != interaction.User.Id)
            {
                await interaction.Interaction.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource,
                    new DiscordInteractionResponseBuilder()
                        .WithContent("Ало, вы шо, ебобо?")
                        .AsEphemeral(true));
                return;
            }

            if (this._paused && interaction.Interaction.Data.CustomId != "toggle-pause")
            {
                await interaction.Interaction.CreateResponseAsync(InteractionResponseType.DeferredMessageUpdate);
            }


            lock (this._lock)
            {
                switch (interaction.Interaction.Data.CustomId)
                {
                    case "double-left":
                        if (!this._game.ModCurrentPosition(new Size(-2, 0)))
                            this._game.ModCurrentPosition(new Size(-1, 0));
                        break;
                    case "left":
                        this._game.ModCurrentPosition(new Size(-1, 0));
                        break;
                    case "right":
                        this._game.ModCurrentPosition(new Size(1, 0));
                        break;
                    case "double-right":
                        if (!this._game.ModCurrentPosition(new Size(2, 0)))
                            this._game.ModCurrentPosition(new Size(1, 0));
                        break;
                    case "rotate-anticlockwise":
                        this._game.Rotate(false);
                        break;
                    case "rotate-clockwise":
                        this._game.Rotate(true);
                        break;
                    case "one-down":
                        this._game.ModCurrentPosition(new Size(0, 1));
                        break;
                    case "down":
                        this._game.FallDown();
                        break;
                    case "toggle-pause":
                        if (this._paused) this.GameResume();
                        else this.GamePause();
                        break;
                    case "rtl":
                        this._rtlMenu = !this._rtlMenu;
                        break;
                }
            }


            await interaction.Interaction.CreateResponseAsync(InteractionResponseType.UpdateMessage,
                new DiscordInteractionResponseBuilder(this.GetMessage().ToMessageBuilder()));
        }

        private void GameStart()
        {
            lock (this._lock)
            {
                this._discordClient.ComponentInteractionCreated += this.OnInteraction;
                this._timer.Elapsed += this.Update;
                this._timer.Start();
            }
        }

        private void GamePause()
        {
            lock (this._lock)
            {
                if (this._paused) return;
                this._paused = true;
                this._pauseLocked = 10;
                this._timer.Stop();
            }
        }

        private void GameResume()
        {
            lock (this._lock)
            {
                if (!this._paused) return;
                this._paused = false;
                this._timer.Start();
            }
        }

        private void GameEnd()
        {
            lock (this._lock)
            {
                this._discordClient.ComponentInteractionCreated -= this.OnInteraction;
                if (this._interaction.GuildId is { } guildId) PlayingGuilds.Remove(guildId);
            }
        }

        private async void Update(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            lock (this._lock)
            {
                if (this._paused || this._game.GameOver) return;
                if (this._pauseLocked > 0) this._pauseLocked--;
                this._game.Tick();
            }

            await this._interaction.EditOriginalResponseAsync(this.GetMessage());
            if (this._game.GameOver) this.GameEnd();
            else this._timer.Start();
        }

        private string DrawMatrix(int[,] matrix)
        {
            var field = new StringBuilder();
            for (var y = 0; y < matrix.GetLength(0); y++)
            {
                for (var x = 0; x < matrix.GetLength(1); x++)
                {
                    field.Append(Emojis[matrix[y, x]]);
                }

                field.Append('\n');
            }

            return field.ToString();
        }

        private DiscordWebhookBuilder GetMessage()
        {
            var message = new DiscordWebhookBuilder();
            var renderedField = this._game.Render();
            if (this._paused) renderedField = renderedField.Apply(v => v > 0 ? 1 : 0);

            var embed = new DiscordEmbedBuilder();

            if (!this._rtlMenu)
            {
                embed = embed.AddField("\u200b", this.DrawMatrix(renderedField), true)
                    .AddField("\u200b", "\u200b", true);
            }

            embed = embed.AddField("\u200b", @$"**Статистика:**

```
Очков: {this._game.Points}
Фигур: {this._game.Tetromino}
Линий: {this._game.Lines}
Тетрисов: {this._game.Tetrises}
```

**След. фигура:**

{this.DrawMatrix(this._game.NextTetromino.GetMatrix(false))}", true);

            if (this._rtlMenu)
            {
                embed = embed.AddField("\u200b", "\u200b", true)
                    .AddField("\u200b", this.DrawMatrix(renderedField), true);
            }

            var color = this._paused ? ButtonStyle.Secondary : ButtonStyle.Primary;
            if (!this._game.GameOver)
                message
                    .AddComponents(
                        new DiscordButtonComponent(color, "double-left", "\u200b", this._paused,
                            new DiscordComponentEmoji(DiscordEmoji.FromUnicode("⏪"))),
                        new DiscordButtonComponent(color, "left", "\u200b", this._paused,
                            new DiscordComponentEmoji(DiscordEmoji.FromUnicode("⬅️"))),
                        new DiscordButtonComponent(color, "right", "\u200b", this._paused,
                            new DiscordComponentEmoji(DiscordEmoji.FromUnicode("➡️"))),
                        new DiscordButtonComponent(color, "double-right", "\u200b", this._paused,
                            new DiscordComponentEmoji(DiscordEmoji.FromUnicode("⏩"))),
                        new DiscordButtonComponent(color, "toggle-pause", "\u200b",
                            !this._paused && this._pauseLocked > 0,
                            new DiscordComponentEmoji(DiscordEmoji.FromUnicode(this._paused ? "▶️" : "⏸️")))
                    )
                    .AddComponents(
                        new DiscordButtonComponent(color, "one-down", "\u200b", this._paused,
                            new DiscordComponentEmoji(DiscordEmoji.FromUnicode("🔽"))),
                        new DiscordButtonComponent(color, "down", "\u200b", this._paused,
                            new DiscordComponentEmoji(DiscordEmoji.FromUnicode("⬇️"))),
                        new DiscordButtonComponent(color, "rotate-anticlockwise", "\u200b", this._paused,
                            new DiscordComponentEmoji(DiscordEmoji.FromUnicode("↪️"))),
                        new DiscordButtonComponent(color, "rotate-clockwise", "\u200b", this._paused,
                            new DiscordComponentEmoji(DiscordEmoji.FromUnicode("↩️"))),
                        new DiscordButtonComponent(color, "rtl", "\u200b", this._paused,
                            new DiscordComponentEmoji(DiscordEmoji.FromUnicode("🔀")))
                    );
            return message
                .AddEmbed(embed);
        }

        [UsedImplicitly]
        [SlashCommand("tetris", "Игра в тетрис")]
        public async Task Command(InteractionContext ctx)
        {
            await this.Game(ctx);
        }

        private async Task Game(BaseContext ctx)
        {
            if (ctx.Interaction.GuildId is { } guildId)
            {
                lock (PlayingGuilds)
                {
                    if (PlayingGuilds.TryGetValue(guildId, out var player))
                    {
                        var _ = ctx.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource,
                            new DiscordInteractionResponseBuilder().WithContent(
                                $"На этом сервере уже ведётся игра. Игрок - {player.Mention}").AsEphemeral(true));
                        return;
                    }

                    PlayingGuilds[guildId] = ctx.User;
                }
            }

            this._interactionId = ctx.Interaction.Id;
            this._interaction = ctx.Interaction;
            this._player = ctx.User;
            this._game = new Tetris(7, 15);
            await ctx.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource,
                new DiscordInteractionResponseBuilder(this.GetMessage().ToMessageBuilder()));
            this.GameStart();
        }
    }
}