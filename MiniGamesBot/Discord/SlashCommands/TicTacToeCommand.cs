using System.Collections.Generic;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.SlashCommands;
using JetBrains.Annotations;
using MiniGamesBot.Discord.Games;
using MiniGamesBot.Discord.Services;
using MiniGamesBot.Extensions;

namespace MiniGamesBot.Discord.SlashCommands
{
    [UsedImplicitly]
    public class TicTacToeCommand : ApplicationCommandModule
    {
        private readonly DiscordClient _client;
        private readonly IQuestionService _questionService;

        public TicTacToeCommand(DiscordClient client, IQuestionService questionService)
        {
            this._client = client;
            this._questionService = questionService;
        }

        private TicTacToe _game = null!;
        private Dictionary<bool, DiscordUser> _players = null!;
        private ulong _interactionId;

        private void GameEnd()
        {
            this._client.ComponentInteractionCreated -= this.OnInteraction;
        }

        private void GameStart()
        {
            this._client.ComponentInteractionCreated += this.OnInteraction;
        }

        private async Task OnInteraction(DiscordClient sender, ComponentInteractionCreateEventArgs interaction)
        {
            if (interaction.Message.Interaction.Id != this._interactionId) return;

            if (interaction.User.Id != this._players[this._game.CurrentPlayer].Id)
            {
                await interaction.Interaction.CreateResponseAsync(
                    InteractionResponseType.ChannelMessageWithSource,
                    new DiscordInteractionResponseBuilder()
                        .WithContent("ТЫ СЛАВА МЕРЛОУ?")
                        .AsEphemeral(true)
                );
                return;
            }

            if (interaction.Interaction.Data.CustomId == "giveUp")
            {
                this._game.SetWinner(!this._game.CurrentPlayer);
                await interaction.Interaction.CreateResponseAsync(InteractionResponseType.UpdateMessage,
                    new DiscordInteractionResponseBuilder(this.GetMessage().ToMessageBuilder()));
            }

            if (!int.TryParse(interaction.Interaction.Data.CustomId, out var id)) return;
            if (!this._game.Move(id))
                await interaction.Interaction.CreateResponseAsync(
                    InteractionResponseType.ChannelMessageWithSource,
                    new DiscordInteractionResponseBuilder()
                        .WithContent("Поле занято")
                        .AsEphemeral(true));
            else
                await interaction.Interaction.CreateResponseAsync(InteractionResponseType.UpdateMessage,
                    new DiscordInteractionResponseBuilder(this.GetMessage().ToMessageBuilder()));
            
            if (!this._game.GameActive) this.GameEnd();
        }

        private DiscordButtonComponent GetButton(int id)
        {
            var (state, player) = this._game.GetFieldState(id);

            var name = player switch
            {
                false => "X",
                true => "O",
                null => "\u200b   \u200b"
            };

            var color = state switch
            {
                TicTacToeFieldState.Free => ButtonStyle.Secondary,
                TicTacToeFieldState.Occupied => ButtonStyle.Primary,
                TicTacToeFieldState.Winning => ButtonStyle.Success,
                _ => ButtonStyle.Secondary
            };

            return new DiscordButtonComponent(color, id.ToString(), name);
        }

        private DiscordWebhookBuilder GetMessage()
        {
            var builder = new DiscordWebhookBuilder()
                .AddComponents(this.GetButton(0b1 << 0), this.GetButton(0b1 << 1), this.GetButton(0b1 << 2))
                .AddComponents(this.GetButton(0b1 << 3), this.GetButton(0b1 << 4), this.GetButton(0b1 << 5))
                .AddComponents(this.GetButton(0b1 << 6), this.GetButton(0b1 << 7), this.GetButton(0b1 << 8))
                .AddComponents(new DiscordButtonComponent(ButtonStyle.Danger, "giveUp", "Сдаться!", !this._game.GameActive));

            if (!this._game.GameActive)
            {
                if (this._game.Winner is { } winner)
                    builder.WithContent(
                        $"Победа {this._players[winner].Mention} ({(winner ? "O" : "X")})");
                else
                    builder.WithContent("Ничья!");
            }
            else
            {
                builder.WithContent(
                    $"Ход {this._players[this._game.CurrentPlayer].Mention} ({(this._game.CurrentPlayer ? "O" : "X")})");
            }

            return builder;
        }

        private async Task Game(BaseContext ctx, DiscordUser opponent)
        {
            this._interactionId = ctx.Interaction.Id;
            
            if (opponent.Id == ctx.Client.CurrentUser.Id)
                opponent = ctx.User;

            if (opponent.IsBot)
                await ctx.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource,
                    new DiscordInteractionResponseBuilder()
                        .WithContent("С ботом играть нельзя. Если не с кем играть - играй сам с собой")
                        .AsEphemeral(true));

            var res = await this._questionService.GameStartQuestion(ctx,
                opponent.Id != ctx.User.Id
                    ? $"{opponent.Mention}, хочешь поиграть в крестики-нолики с {ctx.User.Mention}?"
                    : $"{opponent.Mention}, хочешь поиграть сам с собой?", opponent);
            if (!res)
            {
                this.GameEnd();
                return;
            }

            this._game = new TicTacToe();
            this._players = new Dictionary<bool, DiscordUser>
            {
                {false, ctx.User},
                {true, opponent}
            };
            
            this.GameStart();

            await ctx.Interaction.EditOriginalResponseAsync(this.GetMessage());
        }

        [UsedImplicitly]
        [SlashCommand("tictactoe", "Игра в крестики-нолики")]
        public async Task Command(InteractionContext ctx, [Option("opponent", "Ваш противник")] DiscordUser opponent)
        {
            await this.Game(ctx, opponent);
        }

        [UsedImplicitly]
        [ContextMenu(ApplicationCommandType.UserContextMenu, "Крестики-нолики")]
        public async Task ContextMenu(ContextMenuContext ctx)
        {
            await this.Game(ctx, ctx.TargetUser);
        }
    }
}