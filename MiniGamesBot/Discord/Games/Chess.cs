using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using MiniGamesBot.Extensions;

namespace MiniGamesBot.Discord.Games
{
    public enum PieceType
    {
        King,
        Queen,
        Rook,
        Bischop,
        Knight,
        Pawn,
        Empty
    }

    public class Piece
    {
        private Dictionary<PieceType, int> Prices = new()
        {
            {PieceType.Pawn, 1},
            {PieceType.Knight, 3},
            {PieceType.Bischop, 3},
            {PieceType.Rook, 5},
            {PieceType.Queen, 9}
        };

        public PieceType Type { get; set; }

        /// <summary>
        /// White - true
        /// Black - false
        /// </summary>
        public bool Color { get; }

        public int Moves = 0;
        public int Price => this.Prices.TryGetValue(this.Type, out var price) ? price : 0;

        public Piece(PieceType type, bool color)
        {
            this.Type = type;
            this.Color = color;
        }
    }

    public class Move
    {
        public Point From { get; init; }
        public string Name { get; init; }
        public Point Point { get; init; }
        public bool Transform { get; init; } = false;
        public Dictionary<Point, Point> Moves { get; } = new();
    }

    public enum GameState
    {
        Active,
        PendingTransformation,
        TieRequest,
        Ended
    }

    public class Chess
    {
        public Dictionary<Point, Piece> Pieces { get; private set; } = new();
        public Dictionary<bool, Point> Kings { get; } = new();
        public Dictionary<bool, Point> LastMoves { get; } = new();
        public bool CurrentPlayer = true;

        public Point? SelectedPoint;
        public Dictionary<Point, Piece> AvailablePieces = new();
        public Dictionary<int, Move> AvailableMoves = new();
        public List<KeyValuePair<Point, Piece>> CheckPieces = new();
        public bool IsCheck => this.CheckPieces.Count != 0;
        public GameState GameState;
        public bool? Winner = null;

        public void SetWinner(bool? winner)
        {
            this.GameState = GameState.Ended;
            this.Winner = winner;
        }

        public void OfferTie()
        {
            if (this.GameState == GameState.Active)
            {
                this.GameState = GameState.TieRequest;
                this.CurrentPlayer = !this.CurrentPlayer;
            }
        }

        public void AnswerTieOffer(bool answer)
        {
            if (this.GameState == GameState.TieRequest)
            {
                this.GameState = GameState.Active;
                this.CurrentPlayer = !this.CurrentPlayer;
                if (answer) this.SetWinner(null);
            }
        }

        public bool Transform(PieceType newPiece)
        {
            if (this.GameState != GameState.PendingTransformation) return false;
            if (this.SelectedPoint is not { } point || !this.Pieces.TryGetValue(point, out var piece)) return false;
            piece.Type = newPiece;
            this.AvailableMoves = new Dictionary<int, Move>();
            this.SelectedPoint = null;
            this.GameState = GameState.Active;
            this.UpdatePlayer();
            return true;
        }

        private bool MovePiece(Point from, Point to)
        {
            if (!this.Pieces.TryGetValue(from, out var piece)) return false;
            this.Pieces.Remove(from);
            this.Pieces[to] = piece;

            if (piece.Type == PieceType.King) this.Kings[piece.Color] = to;
            piece.Moves++;
            this.LastMoves[piece.Color] = to;
            return true;
        }

        public bool MovePiece(int moveIndex)
        {
            if (this.SelectedPoint is null || !this.AvailableMoves.TryGetValue(moveIndex, out var move)) return false;
            var backup = this.Pieces.ShallowClone();

            foreach (var (from, to) in move.Moves)
            {
                if (this.MovePiece(from, to)) continue;

                this.Pieces = backup;
                return false;
            }

            if (move.Transform)
            {
                this.GameState = GameState.PendingTransformation;
                this.SelectedPoint = move.Moves.First().Value;
            }
            else
            {
                this.AvailableMoves = new Dictionary<int, Move>();
                this.SelectedPoint = null;
                this.UpdatePlayer();
            }

            return true;
        }

        private static string GetCellName(Point point)
        {
            return $"{char.ConvertFromUtf32(point.X + 'A')}{8 - point.Y}";
        }

        public (float white, float black) GetMaterialPrices()
        {
            var white = this.Pieces
                .Where(p => p.Value.Color)
                .Sum(e => e.Value.Price);
            var black = this.Pieces
                .Where(p => !p.Value.Color)
                .Sum(e => e.Value.Price);
            return (white, black);
        }

        public void UpdatePlayer()
        {
            this.CurrentPlayer = !this.CurrentPlayer;

            var king = this.Kings[this.CurrentPlayer];
            this.CheckPieces = this.Pieces
                .Where(p => p.Value.Color == !this.CurrentPlayer)
                .Where(p => this.CanMovePiece(!this.CurrentPlayer, p.Key, king))
                .ToList();
            this.UpdateAvailablePieces();

            if (this.AvailablePieces.Count == 0)
            {
                this.SetWinner(this.CheckPieces.Count == 0 ? null : !this.CurrentPlayer);
            }
        }

        private List<Move> CalculatePieceMoves(Point point)
        {
            var moves = new List<Move>();
            if (!this.Pieces.TryGetValue(point, out var piece)) return moves;

            for (var y = 0; y < 8; y++)
            {
                for (var x = 0; x < 8; x++)
                {
                    var currentPoint = new Point(x, y);
                    if (this.CanMovePiece(piece.Color, point, currentPoint, true))
                    {
                        var move = new Move
                        {
                            Point = currentPoint,
                            From = point,
                            Name = GetCellName(currentPoint),
                            Moves =
                            {
                                {point, currentPoint}
                            },
                            Transform = piece.Type == PieceType.Pawn && (piece.Color && currentPoint.Y == 0 ||
                                                                         !piece.Color && currentPoint.Y == 7)
                        };
                        moves.Add(move);
                    }
                }
            }

            if (piece.Type == PieceType.King && piece.Moves == 0)
            {
                if (this.CheckPieces.Count == 0 &&
                    this.Pieces.TryGetValue(new Point(7, point.Y), out var rightRook) &&
                    rightRook.Type == PieceType.Rook && rightRook.Moves == 0 &&
                    !this.Pieces.ContainsKey(new Point(6, point.Y)) && !this.Pieces.ContainsKey(new Point(5, point.Y)))
                {
                    moves.Add(new Move()
                    {
                        Name = "O-O",
                        From = point,
                        Moves =
                        {
                            {point, new Point(6, point.Y)},
                            {new Point(7, point.Y), new Point(5, point.Y)}
                        },
                        Point = new Point(6, point.Y)
                    });
                }

                if (this.CheckPieces.Count == 0 &&
                    this.Pieces.TryGetValue(new Point(0, point.Y), out var leftRook) &&
                    leftRook.Type == PieceType.Rook && leftRook.Moves == 0 &&
                    !this.Pieces.ContainsKey(new Point(1, point.Y)) &&
                    !this.Pieces.ContainsKey(new Point(2, point.Y)) &&
                    !this.Pieces.ContainsKey(new Point(3, point.Y)))
                {
                    moves.Add(new Move()
                    {
                        Name = "O-O-O",
                        From = point,
                        Moves =
                        {
                            {point, new Point(2, point.Y)},
                            {new Point(0, point.Y), new Point(3, point.Y)}
                        },
                        Point = new Point(2, point.Y)
                    });
                }
            }

            if (this.CheckPieces.Count == 0)
            {
                var neededHeight = piece.Color ? 3 : 4;
                if (point.Y == neededHeight)
                {
                    var points = new List<Point>();
                    if (point.X > 0) points.Add(new Point(point.X - 1, point.Y));
                    if (point.X < 7) points.Add(new Point(point.X + 1, point.Y));


                    foreach (var currentPoint in points)
                    {
                        if (!this.Pieces.TryGetValue(currentPoint, out var currentPiece)) continue;
                        if (currentPiece.Color == piece.Color) continue;
                        if (currentPiece.Moves != 1 || this.LastMoves[!piece.Color] != currentPoint) continue;
                        var movePoint = new Point(currentPoint.X, piece.Color ? 2 : 5);
                        moves.Add(new Move
                        {
                            Name = GetCellName(movePoint),
                            From = point,
                            Moves =
                            {
                                {currentPoint, movePoint},
                                {point, movePoint}
                            },
                            Point = movePoint
                        });
                    }
                }
            }

            return moves;
        }

        public void SetSelectedPoint(Point? nullablePoint)
        {
            this.SelectedPoint = nullablePoint;
            this.AvailableMoves = new Dictionary<int, Move>();
            if (nullablePoint is not { } point || !this.Pieces.TryGetValue(point, out var piece)) return;
            this.AvailableMoves = this.CalculatePieceMoves(point)
                .Select((e, i) => new KeyValuePair<int, Move>(i, e))
                .ToDictionary(e => e.Key, e => e.Value);
        }

        private static readonly List<PieceType> DefaultPieces = new()
        {
            PieceType.Rook,
            PieceType.Knight,
            PieceType.Bischop,
            PieceType.Queen,
            PieceType.King,
            PieceType.Bischop,
            PieceType.Knight,
            PieceType.Rook
        };

        public void Reset()
        {
            this.Pieces.Clear();
            for (var index = 0; index < DefaultPieces.Count; index++)
            {
                var defaultPiece = DefaultPieces[index];
                var blackPiece = new Piece(defaultPiece, false);
                var whitePiece = new Piece(defaultPiece, true);
                this.Pieces.Add(new Point(index, 0), blackPiece);
                this.Pieces.Add(new Point(index, 1), new Piece(PieceType.Pawn, false));
                this.Pieces.Add(new Point(index, 6), new Piece(PieceType.Pawn, true));
                this.Pieces.Add(new Point(index, 7), whitePiece);

                if (defaultPiece == PieceType.King)
                {
                    this.Kings[true] = new Point(index, 7);
                    this.Kings[false] = new Point(index, 0);
                }
            }

            this.UpdateAvailablePieces();
        }

        public void UpdateAvailablePieces()
        {
            this.AvailablePieces = new Dictionary<Point, Piece>();

            var pieces = this.Pieces.Where(p => p.Value.Color == this.CurrentPlayer).ToList();
            foreach (var piece in pieces)
            {
                var found = false;

                for (var y = 0; y < 8; y++)
                {
                    for (var x = 0; x < 8; x++)
                    {
                        var currentPoint = new Point(x, y);
                        if (this.CanMovePiece(piece.Value.Color, piece.Key, currentPoint, true))
                        {
                            found = true;
                            this.AvailablePieces.Add(piece.Key, piece.Value);
                        }

                        if (found) break;
                    }

                    if (found) break;
                }
            }
        }

        public bool CanMovePiece(bool playerColor, Point from, Point to, bool checkCheck = false)
        {
            if (!this.Pieces.TryGetValue(from, out var sourcePiece)) return false;
            if (playerColor != sourcePiece.Color) return false;
            var isAttack = this.Pieces.TryGetValue(to, out var targetPiece);
            if (isAttack && targetPiece!.Color == playerColor) return false;

            var delta = !playerColor
                ? new Point(to.X - from.X, to.Y - from.Y)
                : new Point(to.X - from.X, from.Y - to.Y);

            if (delta.X == 0 && delta.Y == 0) return false;

            switch (sourcePiece.Type)
            {
                case PieceType.Pawn:
                    if (isAttack)
                    {
                        if (Math.Abs(delta.X) != 1 || delta.Y != 1) return false;
                    }
                    else
                    {
                        if (sourcePiece.Moves > 0)
                        {
                            if (delta.X != 0 || delta.Y is > 1 or < 0) return false;
                        }
                        else
                        {
                            if (delta.X != 0 || delta.Y is > 2 or < 0) return false;
                        }
                    }

                    break;
                case PieceType.Knight:
                    if (!(Math.Abs(delta.X) == 2 && Math.Abs(delta.Y) == 1)
                        && !(Math.Abs(delta.X) == 1 && Math.Abs(delta.Y) == 2)) return false;
                    break;
                case PieceType.Bischop:
                    if (Math.Abs(delta.X) != Math.Abs(delta.Y)) return false;
                    break;
                case PieceType.Rook:
                    if (delta.X * delta.Y != 0) return false;
                    break;
                case PieceType.Queen:
                    if (delta.X * delta.Y != 0 && Math.Abs(delta.X) != Math.Abs(delta.Y)) return false;
                    break;
                case PieceType.King:
                    if (Math.Abs(delta.X) > 1 || Math.Abs(delta.Y) > 1) return false;
                    var board = this.Pieces.ShallowClone();
                    if (isAttack) this.Pieces.Remove(to);
                    foreach (var piece in this.Pieces.Where(e => e.Value.Color == !playerColor))
                    {
                        if (this.CanMovePiece(!playerColor, piece.Key, to, false))
                        {
                            if (isAttack) this.Pieces = board;
                            return false;
                        }
                    }

                    if (isAttack) this.Pieces = board;
                    break;
                default:
                    return false;
            }

            if (sourcePiece.Type != PieceType.Knight)
            {
                var iX = 0;
                var iY = 0;
                while (iX != delta.X || iY != delta.Y)
                {
                    if (iX < delta.X) iX++;
                    if (iX > delta.X) iX--;
                    if (iY < delta.Y) iY++;
                    if (iY > delta.Y) iY--;

                    var id = new Point(from.X + iX, playerColor ? from.Y - iY : from.Y + iY);
                    if (id == to) break;
                    if (this.Pieces.ContainsKey(id)) return false;
                }
            }

            if (checkCheck && this.CurrentPlayer == playerColor && this.CheckPieces.Count != 0)
            {
                var board = this.Pieces.ShallowClone();
                var kings = this.Kings.ShallowClone();
                try
                {
                    if (!this.MovePiece(from, to)) return false;
                    if (this.CheckPieces.Any(checkPiece =>
                        this.CanMovePiece(!playerColor, checkPiece.Key, this.Kings[playerColor])))
                    {
                        return false;
                    }
                }
                finally
                {
                    this.Pieces = board;
                    this.Kings[true] = kings[true];
                    this.Kings[false] = kings[false];
                }
            }

            return true;
            // if (attackingPieces is null) return true;
            // var king = this.FindKing(playerColor);
            // return attackingPieces.All(attackingPiece => !this.CanMovePiece(!playerColor, attackingPiece, king));
        }
    }
}