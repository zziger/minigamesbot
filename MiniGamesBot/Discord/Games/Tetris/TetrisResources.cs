namespace MiniGamesBot.Discord.Games.Tetris
{
    public static class TetrisResources
    {
        public static readonly int[][,] Tetromino =
        {
            // XX
            // XX

            new[,]
            {
                {
                    1, 1
                },
                {
                    1, 1
                }
            },

            // X
            // XXX

            new[,]
            {
                {
                    1, 0, 0
                },
                {
                    1, 1, 1
                },
                {
                    0, 0, 0
                }
            },

            //   X
            // XXX

            new[,]
            {
                {
                    0, 0, 1
                },
                {
                    1, 1, 1
                },
                {
                    0, 0, 0
                }
            },

            //  X
            // XXX

            new[,]
            {
                {
                    0, 1, 0
                },
                {
                    1, 1, 1
                },
                {
                    0, 0, 0
                }
            },

            // XXXX

            new[,]
            {
                {
                    0, 0, 1, 0
                },
                {
                    0, 0, 1, 0
                },
                {
                    0, 0, 1, 0
                },
                {
                    0, 0, 1, 0
                }
            },

            //  XX
            // XX

            new[,]
            {
                {
                    0, 1, 1
                },
                {
                    1, 1, 0
                },
                {
                    0, 0, 0
                }
            },

            // XX
            //  XX 

            new[,]
            {
                {
                    1, 1, 0
                },
                {
                    0, 1, 1
                },
                {
                    0, 0, 0
                }
            }
        };
        
        
    }
}