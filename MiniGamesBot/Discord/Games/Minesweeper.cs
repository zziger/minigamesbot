using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net.Security;
using System.Reflection.Metadata;

namespace MiniGamesBot.Discord.Games
{
    public enum MinesweeperCellState
    {
        Closed,
        Flagged,
        Opened
    }

    public class MinesweeperCell
    {
        public int Value;
        public MinesweeperCellState State;
    }

    public class Minesweeper
    {
        public bool? WonGame;
        public MinesweeperCell[,] Field;
        public bool GeneratedBombs = false;

        public readonly int Width;
        public readonly int Height;
        public readonly int Bombs;
        public int OpenCells { get; private set; }
        private readonly Random _random = new();

        public Minesweeper(int width, int height, int bombs)
        {
            this.Width = width;
            this.Height = height;
            if (width is <= 3 or > 8) throw new ArgumentOutOfRangeException(nameof(width));
            if (height is <= 3 or > 8) throw new ArgumentOutOfRangeException(nameof(height));
            var cells = this.Width * this.Height;
            var maxBombs = cells - 9;
            if (bombs > maxBombs) throw new ArgumentOutOfRangeException(nameof(bombs));
            this.OpenCells = cells;
            this.Bombs = bombs;
            
            
            this.Field = new MinesweeperCell[this.Height, this.Width];
            for (var y = 0; y < this.Height; y++)
            {
                for (var x = 0; x < this.Width; x++)
                {
                    this.Field[y, x] = new MinesweeperCell
                    {
                        Value = 0,
                        State = MinesweeperCellState.Closed
                    };
                }
            }
        }

        private bool ValidateCoords(int x, int y) =>
            x >= 0 && x < this.Field?.GetLength(1) && y >= 0 && y < this.Field?.GetLength(0);

        private void GenerateBombs(int safeX, int safeY)
        {

            for (var i = 0; i < this.Bombs; i++)
            {
                int mineX = this._random.Next(this.Width), mineY = this._random.Next(this.Height);
                while (this.Field[mineY, mineX].Value < 0 || 
                       (mineX >= safeX - 1 && mineX <= safeX + 1 && mineY >= safeY - 1 && mineY <= safeY + 1))
                {
                    mineX = this._random.Next(this.Width);
                    mineY = this._random.Next(this.Height);
                }

                for (var x = -1; x <= 1; x++)
                {
                    for (var y = -1; y <= 1; y++)
                    {
                        if (x == 0 && y == 0)
                        {
                            this.Field[mineY, mineX].Value = -this.Bombs;
                        }
                        else
                        {
                            var fullX = x + mineX;
                            var fullY = y + mineY;
                            if (!this.ValidateCoords(fullX, fullY)) continue;

                            this.Field[fullY, fullX].Value++;
                        }
                    }
                }
            }

            this.GeneratedBombs = true;
        }

        public bool CanBeFlagged(int cellX, int cellY)
        {
            if (!this.ValidateCoords(cellX, cellY)) return false;
            var cell = this.Field[cellY, cellX];
            if (cell.State == MinesweeperCellState.Opened && cell.Value > 0) return this.CanBeOpened(cellX, cellY);
            return cell.State != MinesweeperCellState.Opened;
        }

        public bool CanBeOpened(int cellX, int cellY)
        {
            if (!this.ValidateCoords(cellX, cellY)) return false;
            var cell = this.Field[cellY, cellX];
            if (cell.State == MinesweeperCellState.Flagged) return false;
            if (cell.State == MinesweeperCellState.Opened)
            {
                if (cell.Value > 0)
                {
                    var i = 0;
                    var closed = 0;
                    
                    for (int x = -1; x <= 1; x++)
                    {
                        for (int y = -1; y <= 1; y++)
                        {
                            if (x == 0 && y == 0) continue;
                            var fullX = cellX + x;
                            var fullY = cellY + y;
                            if (!this.ValidateCoords(fullX, fullY)) continue;
                            var currentCell = this.Field[fullY, fullX];
                            if (currentCell.State == MinesweeperCellState.Flagged) i++;
                            if (currentCell.State == MinesweeperCellState.Closed) closed++;
                        }
                    }

                    if (i == cell.Value && closed > 0) return true;
                }

                return false;
            }

            return true;
        }

        public bool Open(int cellX, int cellY)
        {
            if (!this.GeneratedBombs) this.GenerateBombs(cellX, cellY);
            if (!this.ValidateCoords(cellX, cellY)) return false;
            var cell = this.Field[cellY, cellX];
            if (cell.State == MinesweeperCellState.Flagged) return false;
            var wasOpened = cell.State == MinesweeperCellState.Opened;
            cell.State = MinesweeperCellState.Opened;
            if (!wasOpened) this.OpenCells--;

            if (cell.Value < 0)
            {
                this.WonGame = false;
                return true;
            }

            if (cell.Value > 0 && wasOpened)
            {
                var points = new List<Point>();
                var i = 0;
                for (int x = -1; x <= 1; x++)
                {
                    for (int y = -1; y <= 1; y++)
                    {
                        if (x == 0 && y == 0) continue;
                        var fullX = cellX + x;
                        var fullY = cellY + y;
                        if (!this.ValidateCoords(fullX, fullY)) continue;
                        var currentCell = this.Field[fullY, fullX];
                        if (currentCell.State == MinesweeperCellState.Closed) points.Add(new Point(fullX, fullY));
                        if (currentCell.State == MinesweeperCellState.Flagged) i++;
                    }
                }

                if (i != cell.Value) return false;
                foreach (var point in points)
                {
                    this.Open(point.X, point.Y);
                }
            }

            if (cell.Value == 0)
            {
                var points = new List<Point>();
                for (int x = -1; x <= 1; x++)
                {
                    for (int y = -1; y <= 1; y++)
                    {
                        if (x == 0 && y == 0) continue;
                        var fullX = cellX + x;
                        var fullY = cellY + y;
                        if (!this.ValidateCoords(fullX, fullY)) continue;
                        var currentCell = this.Field[fullY, fullX];
                        if (currentCell.Value == 0 && currentCell.State != MinesweeperCellState.Opened)
                            points.Add(new Point(fullX, fullY));
                        if (currentCell.State != MinesweeperCellState.Opened) this.OpenCells--;
                        currentCell.State = MinesweeperCellState.Opened;
                    }
                }
                
                foreach (var point in points)
                {
                    this.Open(point.X, point.Y);
                }
            }

            if (this.OpenCells == this.Bombs)
            {
                this.WonGame = true;
                return true;
            }

            return true;
        }

        public bool Flag(int x, int y)
        {
            if (!this.ValidateCoords(x, y)) return false;
            var cell = this.Field[y, x];
            if (cell.State == MinesweeperCellState.Opened)
            {
                if (cell.Value > 0) return this.Open(x, y);
                return false;
            }
                cell.State = cell.State == MinesweeperCellState.Flagged ? MinesweeperCellState.Closed : MinesweeperCellState.Flagged;
            return true;
        }
    }
}