using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.EventArgs;
using MiniGamesBot.Extensions;

namespace MiniGamesBot.Discord.Services
{
    public interface IComponentInteractionCollector
    {
        Task<ComponentInteractionCreateEventArgs> AwaitInteraction(ulong id, int timeout = 30000);
    }

    public class ComponentInteractionCollector : IComponentInteractionCollector
    {
        public ComponentInteractionCollector(DiscordClient discordClient)
        {
            discordClient.ComponentInteractionCreated += this.OnInteraction;
        }

        private ConcurrentDictionary<ulong, TaskCompletionSource<ComponentInteractionCreateEventArgs>> _sources = new ();
        
        private Task OnInteraction(DiscordClient sender, ComponentInteractionCreateEventArgs e)
        {
            var id = e.Message.Interaction.Id;
            if (!this._sources.TryRemove(id, out var source)) return Task.CompletedTask;
            source.SetResult(e);
            return Task.CompletedTask;
        }

        public Task<ComponentInteractionCreateEventArgs> AwaitInteraction(ulong id, int timeout = 30000)
        {
            var source = new TaskCompletionSource<ComponentInteractionCreateEventArgs>();
            if (!this._sources.TryAdd(id, source)) throw new Exception("Cant add await source");
            return source.Task.TimeoutAfter(TimeSpan.FromMilliseconds(timeout));
        }
    }
}