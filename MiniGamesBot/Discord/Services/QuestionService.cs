using System;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;

namespace MiniGamesBot.Discord.Services
{
    public interface IQuestionService
    {
        Task<bool> GameStartQuestion(BaseContext ctx, string text, DiscordUser opponent);
    }

    public class QuestionService : IQuestionService
    {
        private readonly IComponentInteractionCollector _collector;

        public QuestionService(IComponentInteractionCollector collector)
        {
            this._collector = collector;
        }

        public async Task<bool> GameStartQuestion(BaseContext ctx, string text, DiscordUser opponent)
        {
            await ctx.CreateResponseAsync(
                InteractionResponseType.ChannelMessageWithSource,
                new DiscordInteractionResponseBuilder()
                    .WithContent(text)
                    .AddComponents(
                        new DiscordButtonComponent(ButtonStyle.Success, "yes", "Да"),
                        new DiscordButtonComponent(ButtonStyle.Danger, "no", "Нет"),
                        new DiscordButtonComponent(ButtonStyle.Secondary, "cancel", "Отменить")
                    )
            );

            var gameStarted = false;
            // ReSharper disable once LoopVariableIsNeverChangedInsideLoop
            do
            {
                try
                {
                    var interaction = await this._collector.AwaitInteraction(ctx.InteractionId);
                    var invalidUser = new DiscordInteractionResponseBuilder()
                        .WithContent("Хохла не спрашивали")
                        .AsEphemeral(true);

                    switch (interaction.Interaction.Data.CustomId)
                    {
                        case "cancel":
                            if (interaction.User.Id != ctx.User.Id)
                            {
                                await interaction.Interaction.CreateResponseAsync(
                                    InteractionResponseType.ChannelMessageWithSource,
                                    invalidUser
                                );
                                continue;
                            }

                            await interaction.Interaction.CreateResponseAsync(InteractionResponseType
                                .DeferredMessageUpdate);
                            await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent(text + "\n\nОтменено"));
                            return false;
                        default:
                            if (interaction.User.Id != opponent.Id)
                            {
                                await interaction.Interaction.CreateResponseAsync(
                                    InteractionResponseType.ChannelMessageWithSource,
                                    invalidUser
                                );
                                continue;
                            }

                            await interaction.Interaction.CreateResponseAsync(InteractionResponseType
                                .DeferredMessageUpdate);
                            if (interaction.Interaction.Data.CustomId == "yes")
                            {
                                // ReSharper disable once RedundantAssignment
                                gameStarted = true;
                            }
                            else
                            {
                                await ctx.EditResponseAsync(
                                    new DiscordWebhookBuilder().WithContent(text + "\n\nОтменено"));
                                return false;
                            }

                            break;
                    }
                }
                catch (Exception)
                {
                    await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent(text + "\n\nВремя вышло"));
                }

                break;
            } while (!gameStarted);

            return gameStarted;
        }
    }
}